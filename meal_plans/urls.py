from django.urls import path
from django.contrib.auth import views as auth_views

from meal_plans.views import (
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanUpdateView,
    MealPlanDetailView,
    MealPlanListView,
)


urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plan_list"),
    path(
        "create/",
        MealPlanCreateView.as_view(),
        name="meal_plan_create",
    ),
    path(
        "<int:pk>/",
        MealPlanDetailView.as_view(),
        name="meal_plan_detail",
    ),
    path(
        "<int:pk>/edit/",
        MealPlanUpdateView.as_view(),
        name="meal_plan_update",
    ),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
]
