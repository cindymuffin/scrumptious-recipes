from django import forms
from recipes.models import Rating, Recipe
from django.contrib.auth.models import User


class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "name",
            "author",
            "description",
            "image",
        ]


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]


class SignUpForm(forms.ModelForm):
    class Meta:
        model = User
        fields = [
            "username",
            "email",
            "password",
        ]
